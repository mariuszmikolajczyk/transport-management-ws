package com.hackyeah.transport_management_ws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransportManagmentWsApplicationTests {

    @Test
    public void contextLoads() {
    }

}
