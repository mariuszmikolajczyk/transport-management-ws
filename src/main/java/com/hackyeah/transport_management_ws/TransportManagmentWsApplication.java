package com.hackyeah.transport_management_ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportManagmentWsApplication {

    public static void main(String[] args) {
    	System.setProperty("server.servlet.context-path", "/TransportManager");
        SpringApplication.run(TransportManagmentWsApplication.class, args);
    }
}
