package com.hackyeah.transport_management_ws.repository;

import com.hackyeah.transport_management_ws.entity.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MapRepository extends JpaRepository<Map, Integer> {
}
