package com.hackyeah.transport_management_ws.repository;

import com.hackyeah.transport_management_ws.entity.NodeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeTypeRepository extends JpaRepository<NodeType, Integer> {
}
