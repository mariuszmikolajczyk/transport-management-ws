package com.hackyeah.transport_management_ws.repository;

import com.hackyeah.transport_management_ws.entity.Road;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoadRepository extends JpaRepository<Road, Integer> {

    @Query("select r from Road r where r.map.id=:id")
    List<Road> findAllByMapId(int id);
}
