package com.hackyeah.transport_management_ws.repository;

import com.hackyeah.transport_management_ws.entity.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodeRepository extends JpaRepository<Node, Integer> {

    @Query("select n from Node n where n.map.id=:id")
    List<Node> findAllByMapId(int id);

    @Query("select n from Node n where n.nodeType.name='BUS_STOP'")
    List<Node> findBusStops();
}
