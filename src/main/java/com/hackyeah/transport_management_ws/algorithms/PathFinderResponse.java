package com.hackyeah.transport_management_ws.algorithms;

import java.util.List;

public class PathFinderResponse {

    private final List<Node> inPath;
    private final double inPathLength;
    private final List<Node> outPath;
    private final double outPathLength;

    public PathFinderResponse(List<Node> inPath, double inPathLength, List<Node> outPath, double outPathLength) {
        this.inPath = inPath;
        this.inPathLength = inPathLength;
        this.outPath = outPath;
        this.outPathLength = outPathLength;
    }

    public double getInPathLength() {
        return inPathLength;
    }

    public double getOutPathLength() {
        return outPathLength;
    }

    public List<Node> getInPath() {
        return inPath;
    }

    public List<Node> getOutPath() {
        return outPath;
    }

}
