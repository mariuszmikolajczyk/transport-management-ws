package com.hackyeah.transport_management_ws.algorithms.AStar;

import com.hackyeah.transport_management_ws.algorithms.Node;

import java.util.List;

public class AStarResult {
    private final List<Node> nodes;
    private final double length;

    public AStarResult(List<Node> nodes, double length) {
        this.nodes = nodes;
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public List<Node> getNodes() {
        return nodes;
    }
}
