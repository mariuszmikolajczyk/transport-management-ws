package com.hackyeah.transport_management_ws.algorithms.AStar;

import com.hackyeah.transport_management_ws.algorithms.Graph;
import com.hackyeah.transport_management_ws.algorithms.MovingObjectParams;
import com.hackyeah.transport_management_ws.algorithms.Node;
import com.hackyeah.transport_management_ws.algorithms.Way;

import java.util.*;
import java.util.stream.Stream;

public class AStar {

    private HashMap<Node, HashSet<Node>> openNodes;
    private HashSet<Node> closed;
    private HashMap<Node, Node> cameFrom;
    private HashMap<Node, Double> distance;
    private HashMap<Node, Double> gScore;
    private final Graph graph;
    private Node start;
    private Node end;
    private final MovingObjectParams params;

    public AStar(Graph graph, MovingObjectParams params) {
        this.graph = graph;
        this.params = params;
    }

    public AStarResult solve(int from, int destination) {
        clear();
        initStarEndNodes(from, destination);
        initializeCollections(start);
        initializeOpenSet();
        try{
            return mainLoop();
        }catch(NoSuchElementException e) {
            System.out.println("err");
            return new AStarResult(new ArrayList<>(),0);
        }
    }

    private void initializeCollections(Node start) {
        this.closed = new HashSet<>();
        this.cameFrom = new HashMap<>();
        this.distance = new HashMap<>();
        this.distance.put(start, this.heuristic(start));
        this.gScore = new HashMap<>();
        this.gScore.put(start, 0d);
    }

    private void initStarEndNodes(int from, int destinatino) {
        this.start = this.graph
                .getWays()
                .stream()
                .flatMap(w -> Stream.of(w.getNodeA(), w.getNodeB()))
                .filter(n -> n.getId() == from)
                .findAny()
            .orElseThrow(() ->new RuntimeException("Start point not found"));
        this.end = this.graph
                .getWays()
                .stream()
                .flatMap(w -> Stream.of(w.getNodeA(), w.getNodeB()))
                .filter(n -> n.getId() == destinatino)
                .findAny()
            .orElseThrow(() -> new RuntimeException("End point not found"));
    }

    private void clear() {
        this.openNodes = new HashMap<>();
    }

    private void initializeOpenSet() {
        final HashMap<Node, HashSet<Node>> tmp = new HashMap<>();
        this.graph.getWays().forEach(w -> {
            if(canMoveOn(w))
            if(w.getNodeB().getId() == start.getId()) {
                HashSet<Node> orDefault = tmp.getOrDefault(start.getId(), new HashSet<>());
                orDefault.add(w.getNodeA());
                tmp.put(start, orDefault);
            } else if(w.getNodeA().getId() == start.getId()) {
                HashSet<Node> orDefault = tmp.getOrDefault(start.getId(), new HashSet<>());
                orDefault.add(w.getNodeB());
                tmp.put(start, orDefault);
            }
        });
        this.openNodes = tmp;
    }

    private double calculateDistance(Node a, Node b) {
        return Math.sqrt(Math.pow(a.getPositionX() - b.getPositionX(),2) + Math.pow(a.getPositionX() - b.getPositionX(),2));
    }

    private double heuristic(Node node) {
        return this.calculateDistance(node, this.end);
    }

    private AStar.Pair lowestNode() {
        Map.Entry<Node, HashSet<Node>> entry = this.openNodes.entrySet().iterator().next();
        Node node1 = entry.getKey();
        Node node2 = entry.getValue().iterator().next();
        double dist = this.distance.get(node1) + calculateDistance(node1, node2) + heuristic(node2);
        Iterator<Map.Entry<Node, HashSet<Node>>> iterator = this.openNodes.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<Node, HashSet<Node>> next = iterator.next();
            Iterator<Node> nextIterator = next.getValue().iterator();
            while(nextIterator.hasNext()) {
                Node nod = nextIterator.next();
                double d2 = this.distance.get(next.getKey()) + calculateDistance(next.getKey(), nod) + heuristic(nod);
                if(d2 < dist) {
                    dist = d2;
                    node1 = next.getKey();
                    node2 = nod;
                }
            }
        }

        distance.put(node2, dist);
        gScore.put(node2, gScore.get(node1) + calculateDistance(node1, node2));
        return new AStar.Pair(node1, node2);
    }

    private AStarResult mainLoop() {
        Pair c = lowestNode();
        cameFrom.put(c.b, c.a);
        gScore.put(c.b, calculateDistance(c.a, c.b));
        distance.put(c.b, calculateDistance(c.a, c.b) + heuristic(c.b));

        breakable:
        while(!this.openNodes.isEmpty()) {
            Pair current = lowestNode();
            if(current.a.getId() == end.getId()) {
                List<Node> reconstruct = reconstruct(end);
                return new AStarResult(reconstruct, calculateLength(reconstruct));
            }
            removeFromOpenSet(current.a, current.b);
            closed.add(current.b);

            for(Node nei : getNeighborOfCurrent(current.b)) {
                if(!closed.contains(nei)) {
                    double score = gScore.get(current.b) + calculateDistance(current.b, nei);
                    if(!openNodes.getOrDefault(current.b, new HashSet<>()).contains(nei)) {
                        HashSet<Node> orDefault = openNodes.getOrDefault(current.b, new HashSet<>());
                        orDefault.add(nei);
                        openNodes.put(current.b, orDefault);
                    } else if(score -0.00001> gScore.getOrDefault(nei,Double.MAX_VALUE)) {
                        break breakable;
                    }
                    cameFrom.put(nei, current.b);
                    gScore.put(nei, score);
                    distance.put(nei, score + heuristic(nei));
                }
            }
        }
        List<Node> reconstruct = reconstruct(end);
        return new AStarResult(reconstruct, calculateLength(reconstruct));
    }

    private double calculateLength(List<Node> reconstruct) {
        double s = 0;
        for(int i =0; i< reconstruct.size()-1; ++i) {
            s += calculateDistance(reconstruct.get(i), reconstruct.get(i+1));
        }
        return s;
    }

    private void removeFromOpenSet(Node key, Node value) {
        HashSet<Node> set = openNodes.get(key);
        set.remove(value);
        if(set.isEmpty()) {
            openNodes.remove(key);
        } else {
            openNodes.put(key, set);
        }
    }

    private HashSet<Node> getNeighborOfCurrent(Node node) {
        HashSet<Node> nei = new HashSet<>();
        graph.getWays().stream()
                .filter(this::canMoveOn)
                .filter(e -> e.getNodeA().getId() == node.getId() || e.getNodeB().getId() == node.getId())
                .filter(w -> this.canTurnTo(node, w))
                .forEach(e -> {
            if(e.getNodeA().getId() == node.getId()) {
                nei.add(e.getNodeB());
            } else if(e.getNodeB().getId() == node.getId()) {
                nei.add(e.getNodeA());
            }
        });
        return nei;
    }

    private boolean canMoveOn(Way way) {
        return way.getMaxHeight() >= this.params.getHeight()
                && way.getMaxMass() >= this.params.getMass()
                && way.getMaxWidth() >= this.params.getWidth();
    }

    private boolean canTurnTo(Node from, Way to) {
        Node node = cameFrom.get(from);
        if(node == null) return true;
        Way wayFrom = this.graph
                .getWays()
                .stream()
                .filter(w ->
                        (w.getNodeA().equals(from) && w.getNodeB().equals(node))
                                || (w.getNodeA().equals(node) && w.getNodeB().equals(from)))
                .findFirst().orElseThrow(() -> new RuntimeException("Cannot find way"));
        Node prewNode = wayFrom.getNodeB().getId() == from.getId() ?  wayFrom.getNodeA() : wayFrom.getNodeB();
        if((prewNode.getPositionX() == to.getNodeA().getPositionX() && to.getNodeA().getPositionX() == to.getNodeB().getPositionX())
            || (prewNode.getPositionY() == to.getNodeA().getPositionY() && to.getNodeA().getPositionY() == to.getNodeB().getPositionY())) {
            return true;
        }
        double prewW = wayFrom.getMaxWidth() - this.params.getWidth();
        double nextW = to.getMaxWidth() - this.params.getWidth();
        return this.params.getRadius() <= Math.sqrt(Math.pow(this.params.getRadius() - prewW,2) + Math.pow(this.params.getRadius() - nextW,2));
    }


    private List<Node> reconstruct(Node current) {
        if(cameFrom.containsKey(current) && current.getId() != start.getId()) {
            List<Node> reconstruct = reconstruct(cameFrom.get(current));
            reconstruct.add(current);
            return reconstruct;
        } else {
            List<Node> objects = new ArrayList<>();
            objects.add(start);
            return objects;
        }
    }

    private static class Pair {
        public Node a;
        public Node b;
        public Pair(Node a, Node b) {
            this.a = a;
            this.b = b;
        }
    }
}
