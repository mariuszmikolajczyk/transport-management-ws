package com.hackyeah.transport_management_ws.algorithms;

public class MovingObjectParams {

    private double width;
    private double height;
    private double mass;
    private double radius;

    public MovingObjectParams(){

    }
    public MovingObjectParams(double width, double height, double mass, double radius) {
        this.width = width;
        this.height = height;
        this.mass = mass;
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public double getMass() {
        return mass;
    }

    public double getWidth() {
        return width;
    }

    public double getRadius() {
        return radius;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
}
