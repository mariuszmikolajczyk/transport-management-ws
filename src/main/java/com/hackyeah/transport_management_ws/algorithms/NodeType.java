package com.hackyeah.transport_management_ws.algorithms;

public enum NodeType {
    ROADPOINT,
    BUS_STOP,
    GATE;
}
