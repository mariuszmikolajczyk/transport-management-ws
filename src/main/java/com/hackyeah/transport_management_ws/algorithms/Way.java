package com.hackyeah.transport_management_ws.algorithms;

import java.util.stream.Stream;

public class Way {
    private final Node nodeA;
    private final Node nodeB;
    private final double maxWidth;
    private final double maxHeight;
    private final double maxMass;

    public Way(Node nodeA, Node nodeB, double maxWidth, double maxHeight, double maxMass) {
        this.nodeA = nodeA;
        this.nodeB = nodeB;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
        this.maxMass = maxMass;
    }

    public Node getNodeA() {
        return nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }

    public double getMaxWidth() {
        return maxWidth;
    }
    public double getMaxHeight() {
        return maxHeight;
    }

    public double getMaxMass() {
        return maxMass;
    }

    @Override
    public int hashCode() {
        return Stream.of(nodeA.hashCode(), nodeB.hashCode()).reduce((a, b) -> 21 * a + b).get();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Way) {
            Way that = (Way)obj;
            return that.nodeA.equals(this.nodeA) && this.nodeB.equals(that.nodeB);
        } return false;
    }
}
