package com.hackyeah.transport_management_ws.algorithms;

import com.hackyeah.transport_management_ws.algorithms.AStar.AStar;
import com.hackyeah.transport_management_ws.algorithms.AStar.AStarResult;

import java.util.*;
import java.util.stream.Collectors;

public class RecalculateBusPath {

    private final List<Node> busStops;
    private final Graph graph;
    private final MovingObjectParams params;

    public RecalculateBusPath(List<Node> busStops, Graph graph, MovingObjectParams params) {
        this.busStops = busStops;
        this.graph = graph;
        this.params = params;
    }

    private List<Node> findBusCycle() {
        List<Node> oldStops = new ArrayList<>(busStops);
        List<Node> cycle = new ArrayList<>();
        cycle.add(oldStops.get(0));
        oldStops.remove(0);
        while (!oldStops.isEmpty()) {
            Node last = cycle.get(cycle.size() - 1);
            Node best = oldStops.get(0);
            double dist = distance(last, best);
            for (Node n : oldStops) {
                double d = distance(last, n);
                if (d < dist) {
                    best = n;
                    dist = d;
                }
            }
            cycle.add(best);
            oldStops.remove(best);
        }
        cycle.add(cycle.get(0));
        return cycle;
    }

    private double distance(Node a, Node b) {
        return Math.sqrt(Math.pow(a.getPositionX() - b.getPositionX(), 2) + Math.pow(a.getPositionY() - b.getPositionY(), 2));
    }

    public List<Node> recalculate() {
        List<List<Node>> paths = new ArrayList<>();
        List<Node> cycle = findBusCycle();
        for (int i = 0; i < cycle.size() - 1; ++i) {
            int j = i;
            Graph gr = this.graph;
            if (i > 0 && paths.size() > 0) {
                List<Node> lasts = paths.get(paths.size() - 1);
                if (lasts.size() >= 2) {
                    List<Way> ways = this.graph.getWays()
                            .stream()
                            .filter(w -> !(w.getNodeA().getId() == cycle.get(j).getId() && w.getNodeB().getId() == cycle.get(j + 1).getId())
                                    || !(w.getNodeA().getId() == cycle.get(j+1).getId() && w.getNodeB().getId() == cycle.get(j).getId()))
                            .collect(Collectors.toList());
                    gr = new Graph(ways);
                }
            }
//            if (paths.size() > 0 && i == cycle.size() - 2) {
//                List<Node> lasts = paths.get(0);
//                if (lasts.size() >= 2) {
//                    List<Way> ways = gr.getWays()
//                            .stream()
//                            .filter(w -> !(w.getNodeA().getId() == cycle.get(j).getId() && w.getNodeB().getId() == cycle.get(j + 1).getId())
//                                    || !(w.getNodeA().getId() == cycle.get(j+1).getId() && w.getNodeB().getId() == cycle.get(j ).getId()))
//                            .collect(Collectors.toList());
//                    gr = new Graph(ways);
//                }
//            }
            AStarResult res = new AStar(gr, this.params).solve(cycle.get(i).getId(), cycle.get(i + 1).getId());
            if (!res.getNodes().isEmpty()) {
//                System.out.println("OK");
                paths.add(res.getNodes());
            } else {
//                System.out.println(cycle.get(i).getId() +" "+ cycle.get(i+1).getId());
//                System.out.println("graf");
//                gr.getWays().forEach(e -> System.out.println(e.getNodeA().getId() + " " + e.getNodeB().getId()));

            }
        }
        return paths.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }


    public List<Date> getTimesAtStop(int id) {
        List<Node> recalculate = recalculate();
        if(recalculate.size() == 0) return new ArrayList<>();
        double sum = 0;
        for (int i = 0; i < recalculate.size(); ++i) {
            if (i > 0) {
                sum += distance(recalculate.get(i), recalculate.get(i - 1));
            }
            if (recalculate.get(i).getId() == id) {
                break;
            }
        }
        double all = 0;
        for (int i = 0; i < recalculate.size(); ++i) {
            if (i > 0) {
                all += distance(recalculate.get(i), recalculate.get(i - 1));
            }
        }
        all += distance(recalculate.get(0), recalculate.get(recalculate.size() - 1));

        double v = 30;
        sum /= 1000;
        all /= 1000;
        double offset = sum / v;
        double inter = all / v;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, (int) (offset * 60));
        ArrayList<Date> times = new ArrayList<>();
        System.out.println(sum + " " + all);
        while (cal.get(Calendar.HOUR_OF_DAY) < 18) {
            System.out.println(cal.get(Calendar.HOUR_OF_DAY));
            cal.add(Calendar.MINUTE, (int) (inter * 60));
            times.add(cal.getTime());
            if (times.size() >= 10) break;
        }
        return times;
    }
}
