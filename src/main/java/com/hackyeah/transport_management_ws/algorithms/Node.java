package com.hackyeah.transport_management_ws.algorithms;

import java.util.stream.Stream;

public class Node {
    private final int id;
    private final NodeType type;
    private final double positionX;
    private final double positionY;

    public Node(int id, NodeType type, double positionX, double positionY) {
        this.id = id;
        this.type = type;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public int getId() {
        return id;
    }

    public NodeType getType() {
        return type;
    }

    public double getPositionX() {
        return positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    @Override
    public int hashCode() {
        return Stream.of(id+0.0, positionX, positionY).reduce((a,b) -> 21*a+b).get().intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Node) {
            Node that = (Node)obj;
            return that.positionY == this.positionY && this.positionX == that.positionX && this.id == that.id;
        } return false;
    }
}
