package com.hackyeah.transport_management_ws.algorithms;

import java.util.List;

public class Graph {

    private final List<Way> ways;

    public Graph(List<Way> ways) {
        this.ways = ways;
    }

    public List<Way> getWays() {
        return ways;
    }
}
