package com.hackyeah.transport_management_ws.algorithms.Examples;

import com.hackyeah.transport_management_ws.algorithms.Graph;
import com.hackyeah.transport_management_ws.algorithms.Node;
import com.hackyeah.transport_management_ws.algorithms.NodeType;
import com.hackyeah.transport_management_ws.algorithms.Way;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExampleGraph {
    static Node A = new Node(1, NodeType.ROADPOINT,0,0);
    static Node B = new Node(2, NodeType.ROADPOINT,200,0);
    static Node C = new Node(3, NodeType.ROADPOINT, 200, 400);
    static Node D = new Node(4, NodeType.ROADPOINT,0,400);
    static Node E = new Node(5, NodeType.ROADPOINT,0,700);
    static Node F = new Node(6, NodeType.ROADPOINT,500,700);
    static Node G = new Node(7, NodeType.ROADPOINT,700,800);
    static Node H = new Node(8, NodeType.ROADPOINT,700,0);
    static Node I = new Node(9, NodeType.ROADPOINT,200,-300);
    static Node J = new Node(10, NodeType.ROADPOINT,700,-300);
    static Node K = new Node(11, NodeType.ROADPOINT,-200,400);
    static Node L = new Node(12, NodeType.ROADPOINT,-200,700);
    static Node M = new Node(13, NodeType.ROADPOINT,500,400);
    static Node N = new Node(14, NodeType.ROADPOINT,500,800);
    static Node IN = new Node(15, NodeType.GATE, 100, -300);
    static Node OUT = new Node(16, NodeType.GATE, 0, 900);
    static Node A1 = new Node(17, NodeType.BUS_STOP, 400,0);
    static Node A2 = new Node(18, NodeType.BUS_STOP, 400,700);

    public final static Graph graph = new Graph(new ArrayList<>(Stream.of(
            new Way(A,B,10,1,1),
            new Way(A,D,10,1,1),
            new Way(B,C,10,1,1),
            new Way(B,A1,10,1,1),
            new Way(A1,H,10,1,1),
            new Way(B,I,10,1,1),
            new Way(C,D,9,1,1),
            new Way(C,M,10,1,1),
            new Way(D,K,10,1,1),
            new Way(D,E,10,1,1),
            new Way(E,L,10,1,1),
            new Way(E,A2,10,1,1),
            new Way(A2,F,10,1,1),
            new Way(F,M,10,1,1),
            new Way(F,N,10,1,1),
            new Way(G,H,10,1,1),
            new Way(G,N,10,1,1),
            new Way(H,J,10,1,1),
            new Way(I,J,10,1,1),
            new Way(K,L,10,1,1),
            new Way(IN,I,10,1,1),
            new Way(OUT,E,10,1,1)
            ).collect(Collectors.toList())));

    public static List<Node> bus() {
        return getNodes().stream().filter(e -> e.getType() == NodeType.BUS_STOP).collect(Collectors.toList());
    }

    public static List<Way> getWays() {
        return graph.getWays();
    }

    public static List<Node> getNodes() {
        return Stream.of(A,B,C,D,E,F,G,H,I,J,K,L,M,N,IN,OUT,A1,A2).collect(Collectors.toList());
    }
}
