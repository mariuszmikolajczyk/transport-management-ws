package com.hackyeah.transport_management_ws.algorithms;

import com.hackyeah.transport_management_ws.algorithms.AStar.AStar;
import com.hackyeah.transport_management_ws.algorithms.AStar.AStarResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PathFinder {
    private final Graph graph;
    private final MovingObjectParams params;


    public PathFinder(Graph graph, MovingObjectParams params) {
        this.graph = graph;
        this.params = params;
    }

    public PathFinderResponse solve(int from, int to) {
        AStarResult inPath = getInPath(from, to);
        if(inPath.getNodes().size() >= 2) {
            List<Node> tail = inPath.getNodes().subList(inPath.getNodes().size() - 2, inPath.getNodes().size());
            List<Way> outWays = this.graph
                    .getWays()
                    .stream()
                    .filter(w -> (w.getNodeB().getId() != tail.get(0).getId() || w.getNodeA().getId() != tail.get(1).getId())
                        && (w.getNodeA().getId() != tail.get(0).getId() || w.getNodeB().getId() != tail.get(1).getId()))
                    .collect(Collectors.toList());

            Optional<AStarResult> first = outWays.stream()
                    .flatMap(w -> Stream.of(w.getNodeA(), w.getNodeB()))
                    .filter(n -> n.getType() == NodeType.GATE)
                    .map(n -> new AStar(new Graph(outWays), this.params).solve(to, n.getId()))
                    .filter(c -> !c.getNodes().isEmpty())
                    .findFirst();
            return first.map(nodes -> new PathFinderResponse(inPath.getNodes(), inPath.getLength(), nodes.getNodes(), nodes.getLength()))
                    .orElseGet(() -> new PathFinderResponse(inPath.getNodes(), inPath.getLength(), new ArrayList<>(), 0));
        }
        return new PathFinderResponse(inPath.getNodes(), inPath.getLength(), new ArrayList<>(),0);
    }

    private AStarResult getInPath(int from, int to) {
        return new AStar(this.graph, this.params).solve(from, to);
    }

}
