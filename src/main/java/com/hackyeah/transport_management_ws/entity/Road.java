package com.hackyeah.transport_management_ws.entity;

import javax.persistence.*;

@Entity
@Table(name = "road")
public class Road {

    @Id
    @Column(name = "road_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_node")
    private Node fromNode;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_node")
    private Node toNode;

    @Column
    private double width;

    @Column
    private double height;

    @Column
    private double maxMass;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "map_id")
    private Map map;

    @Column(columnDefinition = "boolean default false")
    private boolean isBlocked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Node getFromNode() {
        return fromNode;
    }

    public void setFromNode(Node fromNode) {
        this.fromNode = fromNode;
    }

    public Node getToNode() {
        return toNode;
    }

    public void setToNode(Node toNode) {
        this.toNode = toNode;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getMaxMass() {
        return maxMass;
    }

    public void setMaxMass(double maxMass) {
        this.maxMass = maxMass;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
