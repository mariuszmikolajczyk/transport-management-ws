package com.hackyeah.transport_management_ws.entity;

import javax.persistence.*;

@Entity
@Table(name = "node_type")
public class NodeType {

    @Id
    @Column(name = "note_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
