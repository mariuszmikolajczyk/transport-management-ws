package com.hackyeah.transport_management_ws.entity;

import javax.persistence.*;

@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @Column(name = "vehicle_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "vehicle_vehicle_id_seq")
    private int id;

    @Column
    private String name;

    @Column
    private double width;
    @Column
    private double height;
    @Column
    private double length;
    @Column(name = "turning_radius")
    private double turningRadius;
    @Column
    private double mass;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getTurningRadius() {
        return turningRadius;
    }

    public void setTurningRadius(double turningRadius) {
        this.turningRadius = turningRadius;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }
}
