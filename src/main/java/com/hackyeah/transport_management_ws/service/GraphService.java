package com.hackyeah.transport_management_ws.service;

import com.hackyeah.transport_management_ws.algorithms.Graph;
import com.hackyeah.transport_management_ws.algorithms.Node;
import com.hackyeah.transport_management_ws.algorithms.NodeType;
import com.hackyeah.transport_management_ws.algorithms.Way;
import com.hackyeah.transport_management_ws.entity.Road;
import com.hackyeah.transport_management_ws.repository.RoadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GraphService {

    private RoadRepository roadRepository;

    @Autowired
    public GraphService(RoadRepository roadRepository) {
        this.roadRepository = roadRepository;
    }

    public Graph createGraphFromDB() {
        List<Way> ways = getWaysFromRoads();
        return new Graph(ways);
    }

    private List<Way> getWaysFromRoads() {
        List<Road> roads = roadRepository.findAll();
        return roads.stream().filter(road -> !road.isBlocked()).
                map(road -> mapRoadToWay(road)).collect(Collectors.toList());
    }

    private Way mapRoadToWay(Road road) {
        Node fromNodeAlg = new Node(road.getFromNode().getId(),
                NodeType.valueOf(road.getFromNode().getNodeType().getName().toUpperCase()), road.getFromNode().getX(),
                road.getFromNode().getY());
        Node toNodeAlg = new Node(road.getToNode().getId(),
                NodeType.valueOf(road.getToNode().getNodeType().getName().toUpperCase()), road.getToNode().getX(),
                road.getToNode().getY());
        return new Way(fromNodeAlg, toNodeAlg, road.getWidth(), road.getHeight(), road.getMaxMass());
    }
}
