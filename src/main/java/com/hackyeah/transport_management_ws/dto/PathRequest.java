package com.hackyeah.transport_management_ws.dto;

import com.hackyeah.transport_management_ws.algorithms.MovingObjectParams;

public class PathRequest {
	private int from;
	private int to;
	private MovingObjectParams vehicle;

	public  PathRequest() {

	}
	public PathRequest(int from, int to, MovingObjectParams vehicle) {
		this.from = from;
		this.to = to;
		this.vehicle = vehicle;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public MovingObjectParams getVehicle() {
		return vehicle;
	}

	public void setVehicle(MovingObjectParams vehicle) {
		this.vehicle = vehicle;
	}

}
