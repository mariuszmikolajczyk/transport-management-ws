package com.hackyeah.transport_management_ws.dto;

import java.util.Date;

public class ErrorDto {
    private Date date;
    private PathRequest request;

    public ErrorDto(){}

    public ErrorDto(Date date, PathRequest request) {
        this.date = date;
        this.request = request;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public void setRequest(PathRequest request) {
        this.request = request;
    }

    public Date getDate() {
        return date;
    }

    public PathRequest getRequest() {
        return request;
    }
}
