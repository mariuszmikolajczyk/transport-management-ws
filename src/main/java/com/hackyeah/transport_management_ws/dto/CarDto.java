package com.hackyeah.transport_management_ws.dto;

public class CarDto {

    private int id;
    private String name;
    private double height;
    private double width;
    private double length;
    private double mass;
    private double turningRadius;

    public CarDto() {
    }

    public CarDto(int id, String name, double height, double width, double length, double mass, double turningRadius) {
        super();
        this.id = id;
        this.name = name;
        this.height = height;
        this.width = width;
        this.length = length;
        this.mass = mass;
        this.turningRadius = turningRadius;
    }

    public CarDto(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getTurningRadius() {
        return turningRadius;
    }

    public void setTurningRadius(double turningRadius) {
        this.turningRadius = turningRadius;
    }

}
