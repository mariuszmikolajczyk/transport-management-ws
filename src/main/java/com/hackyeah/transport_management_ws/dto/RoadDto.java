package com.hackyeah.transport_management_ws.dto;

public class RoadDto {
    private int id;
    private int fromId;
    private int toId;
    private double maxWidth;
    private double maxHeight;
    private double maxMass;
    private boolean blocked;

    public RoadDto() {
    }

    public RoadDto(int fromId, int toId, double maxWidth, double maxHeight, double maxMass) {
        super();
        this.fromId = fromId;
        this.toId = toId;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
        this.maxMass = maxMass;
    }

    public RoadDto(int fromId, int toId, double maxWidth, double maxHeight, double maxMass, int id, boolean blocked) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
        this.maxMass = maxMass;
        this.blocked = blocked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromId() {
        return fromId;
    }

    public int getToId() {
        return toId;
    }

    public double getMaxWidth() {
        return maxWidth;
    }

    public double getMaxHeight() {
        return maxHeight;
    }

    public double getMaxMass() {
        return maxMass;
    }

    public boolean isBlocked() {
        return blocked;
    }
}
