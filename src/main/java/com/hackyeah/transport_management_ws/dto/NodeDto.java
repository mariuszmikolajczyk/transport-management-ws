package com.hackyeah.transport_management_ws.dto;

public class NodeDto {
	private int id;
	private double x;
	private double y;
	private String nodeType;

	public NodeDto(int id, double x, double y, String nodeType) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.nodeType = nodeType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

}
