package com.hackyeah.transport_management_ws.dto;

import java.util.ArrayList;
import java.util.List;

public class MapDto {
	private List<NodeDto> nodes = new ArrayList<>();
	private List<RoadDto> paths = new ArrayList<>();

	public MapDto(List<NodeDto> nodes, List<RoadDto> paths) {
		super();
		this.nodes.addAll(nodes);
		this.paths.addAll(paths);
	}

	public List<NodeDto> getNodes() {
		return nodes;
	}

	public void setNodes(List<NodeDto> nodes) {
		this.nodes = nodes;
	}

	public List<RoadDto> getPaths() {
		return paths;
	}

	public void setPaths(List<RoadDto> paths) {
		this.paths = paths;
	}

}
