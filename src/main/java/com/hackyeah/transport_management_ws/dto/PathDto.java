package com.hackyeah.transport_management_ws.dto;

import java.util.List;

public class PathDto extends MapDto {
	
	public PathDto(List<NodeDto> nodes, List<RoadDto> paths) {
		super(nodes, paths);
	}
	
}
