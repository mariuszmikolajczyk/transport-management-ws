package com.hackyeah.transport_management_ws.controllers;

import com.hackyeah.transport_management_ws.algorithms.Graph;
import com.hackyeah.transport_management_ws.algorithms.MovingObjectParams;
import com.hackyeah.transport_management_ws.algorithms.RecalculateBusPath;
import com.hackyeah.transport_management_ws.dto.NodeDto;
import com.hackyeah.transport_management_ws.repository.NodeRepository;
import com.hackyeah.transport_management_ws.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/busstop")
public class BusStopController {

    private GraphService graphService;
    private NodeRepository nodeRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public BusStopController(GraphService graphService, NodeRepository nodeRepository, ObjectMapper objectMapper) {
        this.graphService = graphService;
        this.nodeRepository = nodeRepository;
        this.objectMapper = objectMapper;
    }

    @GetMapping
    public List<NodeDto> getAll() {
        Graph graph = graphService.createGraphFromDB();
        List<com.hackyeah.transport_management_ws.algorithms.Node> busstopes = objectMapper.mapToNode(nodeRepository.findBusStops());
        MovingObjectParams params = new MovingObjectParams(1, 1, 1, 1);
        RecalculateBusPath recalculateBusPath = new RecalculateBusPath(busstopes, graph, params);
        return objectMapper.mapAlgNodeToNodeDto(recalculateBusPath.recalculate());
    }
}
