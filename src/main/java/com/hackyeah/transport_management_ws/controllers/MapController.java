package com.hackyeah.transport_management_ws.controllers;

import com.hackyeah.transport_management_ws.algorithms.*;
import com.hackyeah.transport_management_ws.dto.*;
import com.hackyeah.transport_management_ws.repository.NodeRepository;
import com.hackyeah.transport_management_ws.repository.RoadRepository;
import com.hackyeah.transport_management_ws.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class MapController {

    private NodeRepository nodeRepository;
    private RoadRepository roadRepository;

    @Value("${map.default.id}")
    private Integer defaultMapId;
    private GraphService graphService;
    private List<DestinationDto> mockDestinations = Stream.of(new DestinationDto(1, "Punkt 1"),
            new DestinationDto(2, "Punkt 2"), new DestinationDto(3, "Punkt 3"), new DestinationDto(4, "Punkt 4"),
            new DestinationDto(5, "Punkt 5"), new DestinationDto(6, "Punkt 6"), new DestinationDto(7, "Punkt 7"),
            new DestinationDto(8, "Punkt 8"), new DestinationDto(9, "Punkt 9"), new DestinationDto(10, "Punkt 10"))
            .collect(Collectors.toList());
    private ObjectMapper objectMapper;

    @Autowired
    public MapController(NodeRepository nodeRepository, RoadRepository roadRepository, GraphService graphService, ObjectMapper objectMapper) {
        this.nodeRepository = nodeRepository;
        this.roadRepository = roadRepository;
        this.graphService = graphService;
        this.objectMapper = objectMapper;
    }

    @GetMapping("/destinations")
    public List<DestinationDto> getDestinations() {
        return nodeRepository.findAllByMapId(defaultMapId).stream().map(n -> new DestinationDto(n.getId(), n.getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/map")
    public MapDto getMap() {
        List<NodeDto> nodes = objectMapper.mapToNodeDto(nodeRepository.findAllByMapId(defaultMapId));
        List<RoadDto> paths = objectMapper.mapToRoadDto(roadRepository.findAllByMapId(defaultMapId));
        return new MapDto(nodes, paths);
    }

//	@GetMapping("/map")
//	@ResponseBody
//	public String getMap() {
//		return mapInJSON;
//	}

    @PostMapping("/path")
    public PathFinderResponse getPath(@RequestBody PathRequest pathRequest) {
        Graph graph = graphService.createGraphFromDB();
        PathFinder pathFinder = new PathFinder(graph, pathRequest.getVehicle());
        return pathFinder.solve(pathRequest.getFrom(), pathRequest.getTo());
    }

    private NodeDto mapNodesToDto(Node node) {
        return new NodeDto(node.getId(), node.getPositionX(), node.getPositionY(), node.getType().toString());
    }

    private RoadDto mapWaysToDto(Way way) {
        return new RoadDto(way.getNodeA().getId(), way.getNodeB().getId(), way.getMaxWidth(), way.getMaxHeight(),
                way.getMaxMass());
    }

//    private NodeDto mapNode(com.hackyeah.transport_management_ws.entity.Node node) {
//        return new NodeDto(node.getId(), NodeType.valueOf(node.getNodeType().getName().toUpperCase()), node.getX(),
//                node.getY());
//    }

}
