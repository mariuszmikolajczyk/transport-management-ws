package com.hackyeah.transport_management_ws.controllers;


import com.hackyeah.transport_management_ws.algorithms.Graph;
import com.hackyeah.transport_management_ws.algorithms.MovingObjectParams;
import com.hackyeah.transport_management_ws.algorithms.RecalculateBusPath;
import com.hackyeah.transport_management_ws.repository.NodeRepository;
import com.hackyeah.transport_management_ws.service.GraphService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/budHours")
public class BusHours {

    private GraphService graphService;
    private ObjectMapper objectMapper;
    private NodeRepository nodeRepository;

    public BusHours(GraphService graphService, ObjectMapper objectMapper, NodeRepository nodeRepository) {
        this.graphService = graphService;
        this.objectMapper = objectMapper;
        this.nodeRepository = nodeRepository;
    }

    @GetMapping()
    public List<Date> getHours(@RequestParam("id") int id) {
        Graph graph = graphService.createGraphFromDB();
        List<com.hackyeah.transport_management_ws.algorithms.Node> busstopes = objectMapper.mapToNode(nodeRepository.findBusStops());
        MovingObjectParams params = new MovingObjectParams(2, 3, 1000, 5);
        RecalculateBusPath recalculateBusPath = new RecalculateBusPath(busstopes, graph, params);
        return recalculateBusPath.getTimesAtStop(id);

    }


}
