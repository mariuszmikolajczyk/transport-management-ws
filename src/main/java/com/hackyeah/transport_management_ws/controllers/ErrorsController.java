package com.hackyeah.transport_management_ws.controllers;


import com.hackyeah.transport_management_ws.algorithms.MovingObjectParams;
import com.hackyeah.transport_management_ws.dto.ErrorDto;
import com.hackyeah.transport_management_ws.dto.PathRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/errors")
public class ErrorsController {

    @GetMapping()
    public List<ErrorDto> getDestinations() {
        return this.fakeErrors();
    }

    private List<ErrorDto> fakeErrors() {
        return Stream.of(
                new ErrorDto(new Date(), new PathRequest(1,2, new MovingObjectParams(12,2,5,5))),
                new ErrorDto(new Date(new Date().getTime() - 100000), new PathRequest(5,17, new MovingObjectParams(12,2,12,5))),
                new ErrorDto(new Date(new Date().getTime() - 200000), new PathRequest(1,2, new MovingObjectParams(12,2,5,5))),
                new ErrorDto(new Date(new Date().getTime() - 300000), new PathRequest(4,7, new MovingObjectParams(11,2,5,9))),
                new ErrorDto(new Date(new Date().getTime() - 400000), new PathRequest(3,5, new MovingObjectParams(1,7,5,5)))
        ).collect(Collectors.toList());
    }
}
