package com.hackyeah.transport_management_ws.controllers;

import com.hackyeah.transport_management_ws.algorithms.NodeType;
import com.hackyeah.transport_management_ws.dto.CarDto;
import com.hackyeah.transport_management_ws.dto.NodeDto;
import com.hackyeah.transport_management_ws.dto.RoadDto;
import com.hackyeah.transport_management_ws.entity.Node;
import com.hackyeah.transport_management_ws.entity.Road;
import com.hackyeah.transport_management_ws.entity.Vehicle;
import com.hackyeah.transport_management_ws.repository.MapRepository;
import com.hackyeah.transport_management_ws.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ObjectMapper {

    @Autowired
    private NodeRepository nodeRepository;
    @Autowired
    private MapRepository mapRepository;
    @Value("${map.default.id}")
    private Integer defaultMapId;

    public Vehicle mapToVehicle(CarDto carDto) {
        Vehicle vehicle = new Vehicle();
        vehicle.setId(carDto.getId());
        vehicle.setHeight(carDto.getHeight());
        vehicle.setWidth(carDto.getWidth());
        vehicle.setLength(0);
        vehicle.setMass(carDto.getMass());
        vehicle.setName(carDto.getName());
        vehicle.setTurningRadius(carDto.getTurningRadius());
        return vehicle;
    }

    Road mapToRoad(RoadDto roadDto) {
        Road road = new Road();
        road.setId(roadDto.getId());
        road.setMaxMass(roadDto.getMaxMass());
        road.setBlocked(roadDto.isBlocked());
        road.setFromNode(nodeRepository.findById(roadDto.getFromId()).get());
        road.setToNode(nodeRepository.findById(roadDto.getToId()).get());
        road.setWidth(roadDto.getMaxWidth());
        road.setHeight(roadDto.getMaxHeight());
        road.setMap(mapRepository.findById(defaultMapId).get());
        return road;
    }

    public List<com.hackyeah.transport_management_ws.algorithms.Node> mapToNode(List<Node> busStops) {
        return busStops.stream().map(n -> new com.hackyeah.transport_management_ws.algorithms.Node(n.getId(), NodeType.valueOf(n.getNodeType().getName()), n.getX(), n.getY())).collect(Collectors.toList());
    }

    public List<NodeDto> mapToNodeDto(List<Node> entities) {
        return entities.stream().map(n -> new NodeDto(n.getId(), n.getX(), n.getY(), n.getNodeType().getName())).collect(Collectors.toList());
    }

    public List<RoadDto> mapToRoadDto(List<Road> entities) {
        return entities.stream().map(r -> new RoadDto(r.getFromNode().getId(), r.getToNode().getId(), r.getWidth(), r.getHeight(), r.getMaxMass(), r.getId(), r.isBlocked())).collect(Collectors.toList());
    }

    public List<NodeDto> mapAlgNodeToNodeDto(List<com.hackyeah.transport_management_ws.algorithms.Node> nodes) {
        return nodes.stream().map(n -> new NodeDto(n.getId(), n.getPositionX(), n.getPositionY(), n.getType().toString())).collect(Collectors.toList());
    }
}
