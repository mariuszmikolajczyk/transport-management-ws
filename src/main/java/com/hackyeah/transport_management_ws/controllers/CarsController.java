package com.hackyeah.transport_management_ws.controllers;

import com.hackyeah.transport_management_ws.dto.CarDto;
import com.hackyeah.transport_management_ws.entity.Vehicle;
import com.hackyeah.transport_management_ws.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/predefinedCars")
public class CarsController {

    private List<CarDto> mockCars = Stream.of(
            new CarDto(1, "Duży samochód"),
            new CarDto(2, "Większy samochód"),
            new CarDto(3, "Największy samochód"),
            new CarDto(4, "Mniejszy samochód"),
            new CarDto(5, "Cysterna")
    ).collect(Collectors.toList());

    private VehicleRepository vehicleRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public CarsController(VehicleRepository vehicleRepository, ObjectMapper objectMapper) {
        this.vehicleRepository = vehicleRepository;
        this.objectMapper = objectMapper;
    }

    @GetMapping
    public List<Vehicle> getPredefiniedCars() {
        return vehicleRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createCar(@RequestBody CarDto carDto) {
        Vehicle carVehicle = objectMapper.mapToVehicle(carDto);
        vehicleRepository.save(carVehicle);
    }

    @PutMapping
    @ResponseStatus(code = HttpStatus.OK)
    public void updateCar(@RequestBody CarDto carDto) {
        Vehicle carVehicle = objectMapper.mapToVehicle(carDto);
        vehicleRepository.save(carVehicle);
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteCar(@RequestParam Integer id) {
        vehicleRepository.deleteById(id);
    }
}
