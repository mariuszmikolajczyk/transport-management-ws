package com.hackyeah.transport_management_ws.controllers;

import com.hackyeah.transport_management_ws.dto.RoadDto;
import com.hackyeah.transport_management_ws.entity.Road;
import com.hackyeah.transport_management_ws.repository.RoadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/road")
public class RoadController {

    private RoadRepository roadRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public RoadController(RoadRepository roadRepository, ObjectMapper objectMapper) {
        this.roadRepository = roadRepository;
        this.objectMapper = objectMapper;
    }

    @GetMapping
    public List<RoadDto> getAll() {
        return roadRepository.findAll().stream().map(r -> new RoadDto(r.getFromNode().getId(), r.getToNode().getId(), r.getWidth(), r.getHeight(), r.getMaxMass(), r.getId(), r.isBlocked())).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.OK)
    public void updateRoad(@RequestBody RoadDto roadDto) {
        Road road = objectMapper.mapToRoad(roadDto);
        roadRepository.save(road);
    }
}
